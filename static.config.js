import React from 'react'
import { createClient } from 'contentful'
import dotenv from 'dotenv'

dotenv.config()

const { CANONICAL_URL, CONTENTFUL_SPACE, CONTENTFUL_ACCESS_TOKEN, AUTHOR_TWITTER_ID } = process.env

const client = createClient({
  space: CONTENTFUL_SPACE,
  accessToken: CONTENTFUL_ACCESS_TOKEN,
})

const SITE_TITLE = '4RCHIV3S'
const SITE_DESCRIPTION = '音楽やWebを作る人間の頭を割って出てきたものたち'

export default {
  plugins: ['react-static-plugin-styled-components'],
  siteRoot: CANONICAL_URL,
  Document: ({ Html, Head, Body, children }) => (
    <Html lang="ja">
      <Head>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        <meta name="twitter:card" content="summary" />
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="/img/apple-touch-icon.png"
        />
        <link rel="icon" href="/img/favicon.ico" />
        <meta property="og:site_name" content={SITE_TITLE} />
        <meta
          property="og:image"
          content={`${CANONICAL_URL}/img/default.jpg`}
        />
        <meta property="og:image:type" content="image/jpeg" />
        <meta property="og:image:width" content="512" />
        <meta property="og:image:height" content="512" />
      </Head>
      <Body>{children}</Body>
    </Html>
  ),
  getSiteData: async () => {
    const { items: categories } = await client.getEntries({
      content_type: 'category',
      order: 'fields.order',
    })
    return {
      title: SITE_TITLE,
      canonicalUrl: CANONICAL_URL,
      description: SITE_DESCRIPTION,
      authorTwitterId: AUTHOR_TWITTER_ID,
      categories,
    }
  },
  getRoutes: async () => {
    const { items: posts } = await client.getEntries({
      content_type: 'post',
      order: '-fields.publishedAt',
    })
    const { items: categories } = await client.getEntries({
      content_type: 'category',
    })
    const { items: about } = await client.getEntries({
      content_type: 'about',
    })
    return [
      {
        path: '/',
        getData: () => ({
          recent: posts[0],
          posts: posts.slice(1),
        }),
        children: categories.map(({ fields, sys }) => ({
          path: fields.name,
          component: 'src/pages/index',
          getData: () => {
            const filtered = posts.filter(
              ({ fields }) => fields.category.sys.id === sys.id
            )
            return {
              recent: filtered[0],
              posts: filtered.slice(1),
            }
          },
          children: posts
            .filter(({ fields }) => fields.category.sys.id === sys.id)
            .map(({ fields }) => ({
              path: fields.articleId,
              component: 'src/containers/Post',
              getData: () => ({
                post: fields,
              }),
            })),
        })),
      },
      {
        path: '/about',
        getData: () => ({
          ...about[0].fields,
        }),
      },
    ]
  },
}
