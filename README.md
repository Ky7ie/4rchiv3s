# 4RCHIV3S

This project is published under the MIT License.

個人ブログ用テンプレートです。

[React Static](https://github.com/nozzle/react-static)を利用してビルドしています。

データソースはHeadlessCMSの[Contentful](https://contentful.com)を利用しています。

MITライセンスで公開しています。

Copyright (c) 2019 solit4ry.io
