import React from 'react'

import Container from 'components/Container'

export default () => (
  <Container>
    <h1>404 - ページないです.</h1>
  </Container>
)
