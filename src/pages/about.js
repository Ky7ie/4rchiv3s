import React from 'react'
import styled from 'styled-components'
import { withRouteData } from 'react-static'

import Container from 'components/Container'
import Media from 'components/Media'

import marked from 'constants/markdown'
import { displayWidth } from 'constants/mediaQuery'
import { BLACK } from 'constants/colors'

const Wrapper = styled.div`
  max-width: ${displayWidth.desktop}px;
  margin: auto;
  padding: 0 16px;
`

const Lead = styled.h1`
  text-align: center;
  letter-spacing: 0.18em;
  margin: 64px 0;
`

const Markdown = styled.div`
  a {
    color: ${BLACK};
    font-weight: 600;
  }

  p {
    font-size: 1rem;
    line-height: 1.8;
  }

  blockquote {
    font-style: italic;
    padding-left: 1em;
    border-left: 2px solid ${BLACK};
    margin-bottom: 32px;
  }
`

const Name = styled.p`
  text-align: center;
  font-weight: 600;
`

const Box = styled.div`
  max-width: 500px;
  margin: auto;
`

export default withRouteData(
  ({
    title,
    content,
    authorName,
    authorImage: { fields: media },
    authorBiography,
  }) => pug`
    Container
      Wrapper
        Lead #{title}
        
        Markdown(dangerouslySetInnerHTML={__html: marked(content)})
        
        Lead Author
        
        Box
          Media(...media)
        
        Name #{authorName}
        
        Markdown(dangerouslySetInnerHTML={__html: marked(authorBiography)})
  `
)
