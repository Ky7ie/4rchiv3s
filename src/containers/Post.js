import React, { Fragment } from 'react'
import { withSiteData, RouteData, Head } from 'react-static'
import styled from 'styled-components'

import Article from 'components/Article'
import Hero from 'components/Hero'
import Container from 'components/Container'
import TwitterLogo from 'components/TwitterLogo'

import { HEADER_REDUCED, HERO_DEFAULT } from 'constants/layouts'

const PostContainer = styled(Container)`
  margin-top: ${HEADER_REDUCED}px;
  min-height: calc(100vh - ${HEADER_REDUCED}px);
`

const Share = styled.div`
  height: ${HERO_DEFAULT}px;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;

  span {
    letter-spacing: 0.28em;
  }

  &.bottom {
    height: 96px;
  }
`

export default withSiteData(({ canonicalUrl }) => (
  <RouteData>
    {({ post }) => {
      const { title, description, publishedAt, category, articleId } = post
      const articlePath = `${category.fields.name}/${articleId}`
      return pug`
        Fragment
          Head
            title #{title}

            meta(name="description" content=description)

            meta(property="og:type" content="article")

            meta(property="og:description" content=description)

            meta(property="og:title" content=title)

            meta(property="og:url" content=canonicalUrl + "/" + articlePath)

            meta(property="article:published_time" content=publishedAt)

          PostContainer
            Hero(...post)

            Share
              span share

              TwitterLogo(articleTitle=title articlePath=articlePath)

            Article(...post)

            Share.bottom
              span share

              TwitterLogo(articleTitle=title articlePath=articlePath)
      `
    }}
  </RouteData>
))
