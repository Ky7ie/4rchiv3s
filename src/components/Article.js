import React from 'react'
import styled from 'styled-components'
import 'highlight.js/styles/dracula.css'

import marked from 'constants/markdown'
import { displayWidth } from 'constants/mediaQuery'
import { BLACK } from 'constants/colors'

const Article = styled.div`
  padding: 16px 4px 0 4px;
  margin: auto;
  word-wrap: break-word;
  max-width: ${displayWidth.desktop}px;

  a {
    color: ${BLACK};
    font-weight: 600;
  }

  p {
    font-size: 1rem;
    margin: 2em 0;
    line-height: 1.8;
  }

  img {
    margin: 2.5em auto;
    display: flex;
    max-width: 100%;
    max-height: 384px;
  }

  hr {
    margin: 5em 0;
    opacity: 0.25;
  }

  h1 {
    margin-top: 2em;
  }

  h2 {
    margin-top: 1.6em;
  }

  pre {
    max-width: 100%;
    overflow: scroll;
  }

  blockquote {
    font-style: italic;
    padding-left: 1em;
    border-left: 2px solid ${BLACK};
  }
`

export default ({ article }) => pug`
  Article(
    dangerouslySetInnerHTML={__html: marked(article)}
  )
`
