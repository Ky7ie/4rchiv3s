import React, { Component } from 'react'
import styled from 'styled-components'
import { CSSTransition } from 'react-transition-group'

import Media from 'components/Media'

import marked from 'constants/markdown'
import { DURATION, EASE_OUT_CIRC } from 'constants/animations'
import { HERO_DEFAULT, HERO_REDUCED, HEADER_Z_INDEX } from 'constants/layouts'
import { WHITE } from 'constants/colors'

const Hero = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: ${HERO_REDUCED}px;
  z-index: ${HEADER_Z_INDEX};
  transition: ${DURATION}ms ${EASE_OUT_CIRC};

  &.top-enter {
    top: ${HERO_REDUCED}px;
    height: ${HERO_DEFAULT}px;
  }

  &.top-enter-done {
    top: ${HERO_REDUCED}px;
    height: ${HERO_DEFAULT}px;
  }

  &.top-appear {
    top: ${HERO_REDUCED}px;
    height: ${HERO_DEFAULT}px;
  }

  &.top-appear-done {
    top: ${HERO_REDUCED}px;
    height: ${HERO_DEFAULT}px;
  }
`

const Title = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  top: 0;
  left: 0;
  font-size: calc(100% + 0.5vw);
  color: ${WHITE};
  span {
    margin: 0 1em;
  }
`

const Copyright = styled.div`
  font-size: 10px;
  transform: scale(0.8);
  transform-origin: right;
  position: absolute;
  opacity: 0.25;
  color: ${WHITE};
  bottom: 4px;
  right: 4px;
  letter-spacing: 1px;

  p {
    margin: 0;
  }
  a {
    color: ${WHITE};
    font-weight: 600;
  }
`

// export default ({ title, headerImage }) => pug`
//   Hero
//     Image(src=headerImage.fields.file.url)
//     p #{title}
// `

export default class extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isScrollOnTop: this.isScrollOnTop,
    }
  }

  componentDidMount() {
    if (typeof window !== 'undefined') {
      window.addEventListener('scroll', this)
    }
  }

  componentWillUnmount() {
    if (typeof window !== 'undefined') {
      window.removeEventListener('scroll', this)
    }
  }

  handleEvent() {
    this.setState({ isScrollOnTop: this.isScrollOnTop })
  }

  get isScrollOnTop() {
    if (typeof window !== 'undefined') {
      return (
        !Math.max(
          window.pageYOffset,
          document.documentElement.scrollTop,
          document.body.scrollTop
        ) > 0
      )
    }
  }

  render() {
    const { title } = this.props
    const { isScrollOnTop } = this.state
    return pug`
      CSSTransition(
      in=isScrollOnTop
      classNames="top"
      timeout=DURATION
      appear
      )
        Hero         
          Media.filtered(...this.props.headerImage.fields)
            
          Title
            span #{title}
          
          if this.props.headerImage.fields.description
            Copyright(dangerouslySetInnerHTML={__html: marked(this.props.headerImage.fields.description)})
    `
  }
}
